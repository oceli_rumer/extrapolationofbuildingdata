import requests as rq
from typing import Optional, Union, List
import json


def _parse_response(response: rq.Response) -> Optional[Union[dict, List[dict]]]:
    if response.status_code != rq.codes.ok:
        if response.status_code == rq.codes.no_content:
            return None
        else:
            response.raise_for_status()
    return response.json()


def send_get_api(path):
    headers = {
        "accept": "application/json",
        "Authorization": "Bearer CWEXFYH2J3K4N6P7Q9SBTBVDWEXGZH2J4M5N6Q8R9SBUCVDXFYGZH3K4M5"
    }
    req = rq.get(path, headers=headers)
    result = _parse_response(req)
    return json.dumps(result, indent=4)
