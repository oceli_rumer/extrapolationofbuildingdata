import pandas as pd
import json
import numpy
from os import listdir, path
from definitions import ROOT_DIR


def main():
    full_data_buildings = dict()
    full_data_buildings['guid'] = []

    for folder in listdir(f'{ROOT_DIR}/CSV/Buildings/'):
        count = [0 for _ in range(5)]
        for i, measure in zip(range(5), listdir(f'{ROOT_DIR}/CSV/Buildings/{folder}')):
            full_path = path.join(f'{ROOT_DIR}/CSV/Buildings/{folder}/', measure)
            if path.isfile(full_path):
                df = pd.read_csv(full_path)

                if len(df.index) > 100000:
                    df.drop(index=[i for i in range(2)], inplace=True)
                    df.index = [i for i in range(len(df.index))]
                    df = df.loc[[i for i in range(len(df.index)) if i % 3 == 0]]
                    df.index = [i for i in range(len(df.index))]

                df.drop(df[numpy.isnan(df['v'])].index, inplace=True)
                count[i] = len(df.index)

        if count[0] == 35136 and count[1] == 35136 and count[2] == 35136 and count[3] == 35136 and count[4] == 34844:
            full_data_buildings['guid'].append(folder)

    with open(f'{ROOT_DIR}/JSON/FullDataBuildings.json', 'w') as file:
        json.dump(full_data_buildings, file, indent=4)


if __name__ == '__main__':
    main()
