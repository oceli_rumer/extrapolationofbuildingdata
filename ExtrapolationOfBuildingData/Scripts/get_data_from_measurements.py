import json
import utils
import pandas as pd
import os
from definitions import ROOT_DIR


def main():
    with open(f'{ROOT_DIR}/JSON/Buildings.json', 'r') as file:
        buildings = json.load(file)

    for building in buildings:
        measurements = json.loads(utils.send_get_api(f'https://api.ecoscada.com/v1/configuration/measurements?buildingGuid={building["guid"]}'))
        for measure in measurements:
            path = f'{ROOT_DIR}/CSV/Buildings/{building["guid"]}'

            if not os.path.exists(path):
                os.makedirs(path)

            if not os.path.exists(f'{path}/{measure["name"]}.csv'):
                measure_data = utils.send_get_api(
                    f'https://api.ecoscada.com/v1/values/measurement/{measure["guid"]}?from=2020-01-01&to=2021-01-01')
                json_file = pd.DataFrame(json.loads(measure_data))
                json_file = json_file.reindex(columns=['ts', 'v'])
                json_file.to_csv(f'{path}/{measure["name"]}.csv', index=False)


if __name__ == '__main__':
    main()
