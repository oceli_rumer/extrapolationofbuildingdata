import utils
from definitions import ROOT_DIR


def main():
    response = utils.send_get_api('https://api.ecoscada.com/v1/configuration/buildings?cugGuid=f07f1fd9-86b7-404c-a0b0-f4f5f964c482')

    with open(f'{ROOT_DIR}/JSON/Buildings.json', "w") as file:
        file.write(response)


if __name__ == '__main__':
    main()
