import pandas as pd
from definitions import ROOT_DIR
from typing import NoReturn, List
from math import sqrt, fabs
from sklearn.metrics import mean_absolute_error, mean_absolute_percentage_error, mean_squared_error


def custom_rmse(observed_data, predicted_data) -> float:
    """
    Функция, находящая квадратный корень из среднеквадратичной ошибки (RMSE)
    :param observed_data: Реальные данные
    :param predicted_data: Прогнозные данные
    :return: Квадратный корень из среднеквадратичной ошибки
    """

    sum = 0
    for i in range(len(observed_data)):
        sum += (predicted_data[i] - observed_data[i]) ** 2
    return sqrt(1 / len(observed_data) * sum)


def custom_mape(observed_data, predicted_data) -> float:
    """
    Функция, находящая среднюю абсолютную ошибку в процентах (MAPE)
    :param observed_data: Реальные данные
    :param predicted_data: Прогнозные данные
    :return: Средняя абсолютная ошибка в процентах
    """

    sum = 0
    for i in range(len(observed_data)):
        if observed_data[i] != 0:
            sum += fabs(predicted_data[i] - observed_data[i]) / observed_data[i]
    return 1 / len(observed_data) * sum


def calculate_rmse(path_to_csv: str, ) -> List[int]:
    df = pd.read_csv(path_to_csv)
    df.drop(columns='DateTime', inplace=True)

    rmse = {}
    best_buildings = []

    # Подсчет rmse
    for i in range(2):
        rmse[f'{df.columns[i]}'] = []
        for j in range(2, 5):
            rmse[f'{df.columns[i]}'].append(custom_rmse(df[df.columns[i]], df[df.columns[j]]))

    # Подсчет индексов наиболее подходящих зданий
    for key, value in rmse.items():
        best_buildings.append(value.index(min(value)) + 2)

    return best_buildings


def glue_data(path_to_week_csv: str, path_to_full_csv: str, indexes: List[int]):
    full_csv = pd.read_csv(path_to_full_csv)
    full_csv.drop(columns='DateTime', inplace=True)
    week_csv = pd.read_csv(path_to_week_csv)
    week_csv.drop(columns='DateTime', inplace=True)

    # week_csv = week_csv.append({full_csv.columns[0]: 1, full_csv.columns[1]: 0.32}, ignore_index=True)
    # Склейка
    for i in range(len(week_csv), len(full_csv.index)):
        week_csv = week_csv.append({measurement_name: full_csv[f'{full_csv.columns[index]}'][i]
                                    for measurement_name, index in
                                    zip(list(week_csv.columns), indexes)},
                                   ignore_index=True)

    print(week_csv)

    for i in range(len(week_csv.columns)):
        # print(week_csv[week_csv.columns[i]])
        # print(full_csv[full_csv.columns[i]])
        print(mean_absolute_error(week_csv[week_csv.columns[i]], full_csv[full_csv.columns[i]]))
        # print('--------------')

    week_csv.to_csv('LookAtMe.csv')


def main() -> NoReturn:
    glue_data(path_to_week_csv=f'{ROOT_DIR}/CSV/TestWeekCSV.csv',
              path_to_full_csv=f'{ROOT_DIR}/CSV/TestCSV.csv',
              indexes=calculate_rmse(f'{ROOT_DIR}/CSV/TestCSV.csv'))


if __name__ == '__main__':
    main()