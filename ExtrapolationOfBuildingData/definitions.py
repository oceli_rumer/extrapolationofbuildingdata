import os
from typing import Final

ROOT_DIR: Final = os.path.dirname(os.path.abspath(__file__))

if __name__ == '__main__':
    print(ROOT_DIR)
